# Microservices
Данный репозиторий представляет собой пример микросервисной системы, выполняющего функции авторизации и поиска неких данных по ключу в базе данных. За авторизацию и поиск данных отвечают разные микросервисы.

# Установка
1. Система написана с использованием Python-фреймворка Flask 0.12.12. Соответственно, чтобы убедиться, что Flask установлен, следующая команда выведет его версию:
```sh
$ flask --version
```
2. Если Flask не установлен, инструкция по установке расположена [здесь](http://flask.pocoo.org/docs/0.12/installation/).
3. Склонировать репозиторий:
```sh
$ cd path_to_your_folder
$ git clone https://MysterySuperhero@bitbucket.org/mysterysuperheroteam/rw-microservices.git
```

# Внимание!
Для корректной работы системы необходимо заполнить базу данных в соответствии с шагами, описанными здесь: [Databasefiller](https://bitbucket.org/mysterysuperheroteam/databasefiller)

# Запуск
После заполнения БД необходимо запустить систему, состоящую из двух микросервисов, выполнив следующие команды в терминале (предполагается, что вы находитесь в папке приложения ../microservices):
Запуск микросервиса авторизации:
```sh
$ export FLASK_APP=/path_to_folder/microservices/authorization/authorization.py
$ flask run -p 6000 # запуск приложения на 6000 порту
```
Запуск микросервиса поиска данных:
```sh
$ export FLASK_APP=/path_to_folder/microservices/general/general.py
$ flask run -p 7000 # запуск приложения на 7000 порту
```
