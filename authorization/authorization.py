import requests
from flask import Flask
from flask import json
from flask import request

import db_connector
import helpers

app = Flask(__name__)


@app.route('/object/get/', methods=['POST'])
def authorize():
    params = request.json
    connection = db_connector.connect()

    try:
        helpers.check_params(params, ["username", "password", "object"])

        username = params["username"]
        password = params["password"]
        object_key = params["object"]

        query = "SELECT username, password FROM users WHERE username = %s AND password = %s;"
        params = (username, password)
        user = db_connector.select_query(connection, query, params)
        connection.close()

        if not user:
            return json.dumps({"code": 1, "response": "User not found"})

        r = requests.post(
            "http://127.0.0.1:7000/object/get/",
            json={'object': object_key}
        )

        if r.status_code == 200:
            return json.dumps({"code": 0, "response": "OK"})

    except Exception as e:
        return json.dumps({"code": 1, "response": e})

    return json.dumps({"code": 0, "response": "ERROR"})


if __name__ == '__main__':
    app.run(port='6000')
