from flask import Flask
from flask import json
from flask import request

import db_connector
import helpers

app = Flask(__name__)


@app.route('/object/get/', methods=['POST'])
def get_object():
    params = request.json
    connection = db_connector.connect()

    try:
        helpers.check_params(params, ["object"])

        object_key = params["object"]

        query = "SELECT value FROM objects_and_values WHERE object= %s;"
        params = (object_key, )
        value = db_connector.select_query(connection, query, params)

        if not value:
            return json.dumps({"code": 1, "response": "Object not found"})

    except Exception as e:
        return json.dumps({"code": 1, "response": e})

    return json.dumps({"code": 0, "response": value[0][0]})


if __name__ == '__main__':
    app.run(port='7000')
